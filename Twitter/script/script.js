const container = document.querySelector('.container');
const USER_URL = "https://ajax.test-danit.com/api/json/users";
const POST_URL = "https://ajax.test-danit.com/api/json/posts";

const loader = document.querySelector('.loader');

class Card {
  constructor(user, posts) {
    this.user = user;
    this.posts = posts;
  }
  getUserCard() {
    const box = document.createElement('div');
    box.classList.add('box');
    const name = this.user.name;
    const email = this.user.email;
    const titleLinkBox = document.createElement('div');
    titleLinkBox.classList.add('title-link-box');
    const title = document.createElement('h2');
    title.innerText = name;
    title.classList.add('title');
    const link = document.createElement('a');
    link.classList.add('link');
    link.innerText = email;

    const deleteButton = document.createElement('button');
    deleteButton.classList.add('delete-btn');
    deleteButton.addEventListener('click', () => {
      deletePostById(this.user.id);
    });

    const addPostButton = document.createElement('button');
    addPostButton.classList.add('addPost-btn');
    addPostButton.addEventListener('click', () => {
      const form = document.createElement('form');
      form.classList.add('form');
      
      const bodyInput = document.createElement('input');
      bodyInput.type = 'text';
      bodyInput.placeholder = 'Enter post body';
      
      const saveButton = document.createElement('button');
      saveButton.classList.add('submit')
      saveButton.setAttribute('type', 'submit');
      saveButton.innerText = 'Save';
      
      form.append(bodyInput, saveButton);
      box.append(form);
      
      form.addEventListener('submit', (event) => {
        event.preventDefault();
        const updatedBody = bodyInput.value;
        const updatedPostId = 1;
        const updatedPostData = { body: updatedBody };
        fetch(`${POST_URL}/${updatedPostId}`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(updatedPostData),
        })
        .then(response => response.json())
        .then(updatedPost => {
          console.log("Updated post data:", updatedPost);
          postContent.innerHTML = '';
          const postSpan = document.createElement('span');
          postSpan.classList.add('post');
          postSpan.innerText = updatedPost.body;
          postContent.append(postSpan);
          form.remove();
        })
        .catch(error => console.error('Error updating post:', error));
      });
    });
    
      
    const buttonContainer = document.createElement('div');

    const bs = this.user.company.catchPhrase;
    const busines = document.createElement('p');
    busines.classList.add('busines');
    busines.innerText = bs;
    
    const postContent = document.createElement('p');
    postContent.classList.add('post-content')

    let postLoaded = 0;
    this.posts.forEach(postItem => {
      if (this.user.id === postItem.userId) {
        const postTitle = postItem.title;
        const postSpan = document.createElement('span');
        postSpan.classList.add('post');
        postSpan.innerText = postTitle;
        postContent.append(postSpan);
        postLoaded++;
      }
    });
    buttonContainer.append(addPostButton, deleteButton);
    titleLinkBox.append(title, link, buttonContainer);
    box.append(titleLinkBox, busines, postContent);
    box.setAttribute('id', this.user.id); 

    return box;
  }
}

function getUsers() {
  loader.style.display = 'block';
  Promise.all([fetch(POST_URL).then(response => response.json()), fetch(USER_URL).then(response => response.json())])
    .then(data => {
      const posts = data[0];
      console.log(posts)
      const users = data[1];
      console.log(users)
      container.innerHTML = '';
      users.forEach(user => {
        const userCard = new Card(user, posts);
        container.append(userCard.getUserCard());
      });

      setTimeout(() => {
        loader.style.display = 'none';
      }, 700)
    })
    .catch(error => {
      console.error('Error:', error);
      loader.style.display = 'none';
    });
}

class Add {
  constructor(title, body) {
    this.title = title;
    this.body = body;
    this.titleInput = null;
    this.bodyInput = null;
  }

  addCards() {
    const addButton = document.createElement("button");
    addButton.classList.add('add-btn');
    addButton.innerText = "POST";
    document.body.prepend(addButton);
    addButton.addEventListener('click', () => {
      const form = document.createElement('form');
      form.classList.add('form');
      this.titleInput = document.createElement('input');
      this.titleInput.setAttribute('type', 'text');
      this.titleInput.setAttribute('placeholder', 'Title');
      this.bodyInput = document.createElement('textarea');
      this.bodyInput.setAttribute('placeholder', 'Text');
      const submit = document.createElement('button');
      submit.setAttribute('type', 'submit');
      submit.classList.add('submit');
      submit.innerText = 'Submit';

      form.append(this.titleInput, this.bodyInput, submit);
      document.body.append(form);

      form.addEventListener('submit', (e) => {
        e.preventDefault();
        this.title = this.titleInput.value;
        this.body = this.bodyInput.value;
        this.addPostById();
        form.remove(); 
      });
    });
  }

  addPostById() {
    fetch(POST_URL, {
      method: 'POST',
      body: JSON.stringify({
        title: this.title,
        body: this.body,
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(response => response.json())
    .then(json => {
      const dataContainer = document.createElement('div');
      dataContainer.classList.add('data-container');
      const addTitle = document.createElement('h2');
      addTitle.classList.add('add-title');
      const addText = document.createElement('p');
      addText.classList.add('add-text');
      addTitle.innerHTML = this.title;
      addText.innerHTML = this.body;
      dataContainer.append(addTitle, addText);
      document.body.prepend(dataContainer);
    })
    .catch(error => console.log('Error:', error));
  }
}

const myAdd = new Add();
myAdd.addCards();

function deletePostById(id){
  fetch(`${POST_URL}/${id}`, {
      method: 'DELETE'
  }).then((resp) => {
    if(resp.status === 200){
      const deletedUserCard = document.getElementById(`${id}`);
      deletedUserCard.remove();
    }else{
      console.error('failed to delete user');
    }
  })
    .catch((error) => console.log(error))
}
getUsers();

// function handleAddPostClick(posts, userId) {
//   const updatedBody = prompt('Enter updated body', posts.body);
//   const updatedPost = {
//     body: updatedBody
//   };
//   editAppointmentById(userId, updatedPost);
// }

// function editAppointmentById(id, newAppointment) {
//   fetch(`${POST_URL}/${id}`, {
//     method: "PUT",
//     headers: {
//       "Content-Type": "application/json",
//     },
//     body: JSON.stringify(updatedPostData),
//   })
//   .then((resp) => {
//     if (resp.status === 200) {
//       console.log(resp)
//     } else {
//       throw new Error('Failed to update post');
//     }
//   })
//   .catch((error) => console.log(error));
// }


